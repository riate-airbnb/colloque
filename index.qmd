---
title: "Locations meublées touristiques <br> dans le Grand Paris "
subtitle: "Colloque RIATE - Atelier Parisien d'Urbanisme - 29 septembre 2023"
lang: "FR"
format:
  html:
    css: "docs/css/styles.css"
    embed-resources: true
    favicon: "docs/logo.png"
resources: 
- "docs"
---

<center>
![](docs/logo.jpg)
</center>

<br>

![](docs/actifs_2022.png)

### Quels outils d'observation communs ?

::: {.big}

Dans un contexte de **crise du logement** exacerbée pour les habitants des métropoles, les locations meublées touristiques participent avec les logements vacants et les résidences secondaires à la raréfaction du parc de logements.

Pour accompagner les politiques publiques, la **production d’indicateurs et d’analyses** de suivi est essentielle afin d’objectiver le nombre d’annonces et de locations réalisées, de prendre en compte les tendances récentes et d’identifier les secteurs géographiques les plus concernés. Au sein de la métropole parisienne, les initiatives en ce sens sont nombreuses, issues d’acteurs publics institutionnels et collectivités territoriales (Ville de Paris, Métropole du Grand Paris, communes, territoires), des agences d’urbanisme ou du monde de la recherche. 

Ces initiatives produisent des analyses, souvent ponctuelles et sur une partie du territoire francilien. La mise en œuvre d’une **observation pérenne soulève de nombreuses difficultés** à surmonter : tout d’abord, des enjeux relatifs à l’accès et au **partage de données** des plateformes de location, onéreuses ou juridiquement sensibles (webscraping, RGPD, restrictions ELAN…). Elle soulève aussi la question des **méthodes de traitement de ces données** afin de produire des indicateurs communs et des outils de visualisation fiables et utiles à la prise de décision dans la durée. 

Cet événement organisé le **29 septembre 2023** par l'UAR RIATE (CNRS - Université Paris Cité), l’Atelier parisien d’urbanisme (Apur) et Jeanne Richon (Université Paris-Est Créteil, Lab’Urba, EIREST) a permis de faire état de ces enjeux dans le Grand Paris en présentant des réflexions et travaux d’analyse récents. Cet événement a également permis de discuter de l’**opportunité de mise en œuvre d’un outil de suivi pérenne**.

Le colloque a rassemblé **110 participants** issus des collectivités territoriales (communes, Établissements Publics Territoriaux, Métropole du Grand Paris), des services de l’Etat, des agences d’urbanisme, des acteurs associatifs et du milieu universitaire et de la recherche. Il a bénéficié du soutien de la Mairie de Paris et de la Métropole du Grand Paris.

Les présentations et captations audios sont disponibles ci-dessous. 

::: 

---

### Ouverture

**Blanche Guillemot**, directrice du logement et de l’habitat (Ville de Paris)  
**Marianne Guérois**, maîtresse de conférences (Université Paris Cité, Géographie-cités, RIATE)  
**Alexandre Labasse**, directeur général (Apur)  

<center>
<audio controls="" preload="none" src="https://api.nakala.fr/data/10.34847/nkl.398dy08k/ce983b4d1ca39465cc5d80cd8f2a691eafb18e24" type="audio/mpeg" data-external="1"></audio>

<p></p>

:::: {.columns}

::: {.column width="49%"}

![](docs/img/IMG_20230929_141613.jpg){width=100%}

::: 

::: {.column width="2%"}

:::

::: {.column width="49%"}

![](docs/img/IMG_20230929_140950.jpg)
::: 

::::

---

### Quelles sources de données pour analyser le phénomène Airbnb dans la région parisienne ?

**Jeanne Richon**, doctorante (Université Paris-Est Créteil, Lab’Urba, EIREST)

<center>
<audio controls="" preload="none" src="https://api.nakala.fr/data/10.34847/nkl.398dy08k/3066ec0f5f52c6edd5afe8aa009f40b97eedac63" type="audio/mpeg" data-external="1"></audio>

<p></p>

![](docs/img/IMG_20230929_142102.jpg){width=50%}
</center>

---

### Présentation d’une plateforme de visualisation du marché de type Airbnb et ses évolutions récentes en Île-de-France

**Louis Laurian**, ingénieur en sciences de l'information géographique (RIATE, CNRS)  
**Ronan Ysebaert**, ingénieur en sciences de l'information géographique (RIATE - Université Paris Cité)  
**Marianne Guérois**, maîtresse de conférences (Université Paris Cité, Géographie-cités, RIATE)  
**Malika Madelin**,   maîtresse de conférences (Université Paris Cité, PRODIG)  

<center>
<audio controls="" preload="none" src="https://api.nakala.fr/data/10.34847/nkl.398dy08k/fe019bf8f6d2a03c490012f68c4cea9a7b20ae63" type="audio/mpeg" data-external="1"></audio>
</center>

<center>
<a href="https://api.nakala.fr/embed/10.34847/nkl.398dy08k/b38fee642fa5e55aca8c029dcaf67278b78852a7" target="_blank">Voir la présentation (.pdf)</a> // <a href="https://riate-airbnb.gitpages.huma-num.fr/website/" target="_blank">Accéder au site Web</a>
</center>

<p></p>

:::: {.columns}

::: {.column width="49%"}

![](docs/img/IMG_20230929_143307.jpg)

:::

::: {.column width="2%"}
:::

::: {.column width="49%"}
![](docs/img/IMG_20230929_144155.jpg)
::: 

:::: 

---

### Table ronde 1 - Quelles tendances pour les locations meublées touristiques dans le Grand Paris ?

***Animation*** : *Stéphanie Jankel, directrice des études (Apur) et Marianne Guérois,  maîtresse de conférences (Université Paris Cité, Géographie-cités, RIATE)*

**Charles-Henri Boisseau**, data analyst (Office du Tourisme et des Congrès de Paris)  
**Corentin Ortais**, urbaniste (Atelier parisien d'urbanisme)  
**Emmanuel Trouillard**, géographe (Institut Paris Région)  
 
<center>
<audio controls="" preload="none" src="https://api.nakala.fr/data/10.34847/nkl.398dy08k/20b26ed97e4ecf79e2794d1ac4e3b942ba24fa03" type="audio/mpeg" data-external="1"></audio>
</center>

<center>
<a href="https://api.nakala.fr/embed/10.34847/nkl.398dy08k/d1a71e19cccd9d9dff075a91ee0361ae2192794f" target="_blank">Voir les visuels associés</a>
</center> 
<p></p>

:::: {.columns}

::: {.column width="49%"}

![](docs/img/IMG_20230929_150307.jpg)

:::

::: {.column width="2%"}
:::

::: {.column width="49%"}
![](docs/img/IMG_20230929_150900.jpg)
:::

:::: 

---


### Table ronde 2 - Quelles perspectives pour mutualiser des données et créer un outil d’observation ?

***Animation*** : *Stéphanie Jankel, directrice des études (Apur) et Jeanne Richon, doctorante (Université Paris-Est Créteil)*

**Gwénolé Buck**, chargé de mission (Direction de l'Habitat, de l'Urbanisme et des Paysages)  
**Murray Cox**, créateur de la plateforme InsideAirbnb  
**Anne Noël**, directrice de l'Urbanisme réglementaire et **Aurélie Jubert**, responsable du service Enseignes, publicité et meublés touristiques (Établissement Public Territorial de Plaine Commune)  
**Sarah Yousfi**, responsable du service Habitat et développement durable (ville de Courbevoie)  
**Amandine Zancanaro**, adjointe au chef de bureau de la protection des locaux d'habitation (ville de Paris)  


<center>
<audio controls="" preload="none" src="https://api.nakala.fr/data/10.34847/nkl.398dy08k/31122eb333ab41023b917a5d8d1bc4fd3d7fff3e" type="audio/mpeg" data-external="1"></audio>
</center>
<p></p>

:::: {.columns}

::: {.column width="49%"}

![](docs/img/IMG_20230929_163025.jpg)

::: 

::: {.column width="2%"}
:::

::: {.column width="49%"}
![](docs/img/1696587995563.jpg)
:::

::::
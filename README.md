## Locations meublées touristiques dans le Grand Paris

**Colloque RIATE - Atelier Paris d'Urbanisme - 29 septembre 2023 - Auditorium Totem**

Descriptif de la journée d'étude, captations vidéos et présentations. Disponible [**ici**](https://riate-airbnb.gitpages.huma-num.fr/colloque/) 

![](docs/img/IMG_20230929_145512.jpg)


[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-sa/4.0/)


![](docs/logo.jpg)